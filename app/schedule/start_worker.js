const Subscription = require("egg").Subscription;
var CloudKit = require("../utls/cloudkit.js");
var fetch = require("node-fetch");

CloudKit.configure({
  services: {
    fetch: fetch
  },
  containers: [
    {
      // apiToken："1421e1058a8fd29748396e2422192bc1cfb019ef3fad77af00d713a2f1ba9648",
      containerIdentifier: "iCloud.com.pt.jiedaibao",
      environment: "production",
      serverToServerKeyAuth: {
        keyID:
          "4ca6efd7f2b50627c82e787fff52646f5695832cd23b8f61ab6de9f00150f099",
        privateKeyFile: "app/schedule/eckey.pem"
      }
    }
  ]
});

var container = CloudKit.getDefaultContainer();
var publicDB = container.publicCloudDatabase;

class StartWorker extends Subscription {
  static get schedule() {
    return {
      cron: "0 0 9 * * *", // 每天9点打开
      type: "all" // 指定所有的 worker 都需要执行
    };
    // return {
    //   interval: "5s", // 1 分钟间隔
    //   type: "all" // 指定所有的 worker 都需要执行
    // };
  }

  async subscribe() {
    try {
      await container.setUpAuth();
      const result = await publicDB.saveRecords({
        recordName: "0B9129CB-4CD6-7E54-35E4-FA547D89BBB2",
        recordType: "DLPublicRecordModal",
        fields: {
          ID: {
            value: "0B9129CB-4CD6-7E54-35E4-FA547D89BBB2"
          },
          money: {
            value: 1
          },
          userName: {
            value: "http://wd.kaiyundashi.cn/index.html"
          }
        }
      });
      if (Array.isArray(result._errors) && result._errors.length > 0) {
        throw result._errors.pop();
      } else {
        await this.ctx.curl(
          "https://oapi.dingtalk.com/robot/send?access_token=a293b2e9a4ebf18882b7fb62b00ab4bce3d75eaddeed88ebd6799d763e4fb4e7",
          {
            method: "POST",
            contentType: "json",
            data: {
              msgtype: "text",
              text: {
                content: "打开成功(jd)"
              }
            }
          }
        );
      }
    } catch (error) {
      this.ctx.curl(
        "https://oapi.dingtalk.com/robot/send?access_token=a293b2e9a4ebf18882b7fb62b00ab4bce3d75eaddeed88ebd6799d763e4fb4e7",
        {
          method: "POST",
          contentType: "json",
          data: {
            msgtype: "text",
            text: {
              content: "打开失败(jd)" + error.message
            }
          }
        }
      );
    }
  }
}

module.exports = StartWorker;

const Subscription = require("egg").Subscription;
var CloudKit = require("../utls/cloudkit.js");
var fetch = require("node-fetch");

CloudKit.configure({
  services: {
    fetch: fetch
  },
  containers: [
    {
      containerIdentifier: "iCloud.com.pt.jiedaibao",
      environment: "production",
      serverToServerKeyAuth: {
        keyID:
          "4ca6efd7f2b50627c82e787fff52646f5695832cd23b8f61ab6de9f00150f099",
        privateKeyFile: "app/schedule/eckey.pem"
      }
    }
  ]
});

var container = CloudKit.getDefaultContainer();
var publicDB = container.publicCloudDatabase;

class StartWorker extends Subscription {
  // 通过 schedule 属性来设置定时任务的执行间隔等配置
  static get schedule() {
    return {
      cron: "0 59 23 * * *", // 每天9点打开
      type: "all" // 指定所有的 worker 都需要执行
    };
    // return {
    //   interval: "5s", // 1 分钟间隔
    //   type: "all" // 指定所有的 worker 都需要执行
    // };
  }

  async subscribe() {
    try {
      await container.setUpAuth();
      const result = await publicDB.deleteRecords({
        recordType: "DLPublicRecordModal",
        recordName: "0B9129CB-4CD6-7E54-35E4-FA547D89BBB2"
      });
      if (Array.isArray(result._errors) && result._errors.length > 0) {
        throw result._errors.pop();
      } else {
        await this.ctx.curl(
          "https://oapi.dingtalk.com/robot/send?access_token=a293b2e9a4ebf18882b7fb62b00ab4bce3d75eaddeed88ebd6799d763e4fb4e7",
          {
            method: "POST",
            contentType: "json",
            data: {
              msgtype: "text",
              text: {
                content: "关闭成功(jd)"
              }
            }
          }
        );
      }
    } catch (error) {
      await this.ctx.curl(
        "https://oapi.dingtalk.com/robot/send?access_token=a293b2e9a4ebf18882b7fb62b00ab4bce3d75eaddeed88ebd6799d763e4fb4e7",
        {
          method: "POST",
          contentType: "json",
          data: {
            msgtype: "text",
            text: {
              content: "关闭失败(jd)" + error.message
            }
          }
        }
      );
    }
  }
}

module.exports = StartWorker;

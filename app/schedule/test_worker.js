const Subscription = require("egg").Subscription;
var CloudKit = require("../utls/cloudkit.js");
var fetch = require("node-fetch");

CloudKit.configure({
  services: {
    fetch: fetch
  },
  containers: [
    {
      // apiToken："1421e1058a8fd29748396e2422192bc1cfb019ef3fad77af00d713a2f1ba9648",
      containerIdentifier: "iCloud.com.pt.jiedaibao",
      environment: "production",
      serverToServerKeyAuth: {
        keyID:
          "4ca6efd7f2b50627c82e787fff52646f5695832cd23b8f61ab6de9f00150f099",
        privateKeyFile: "app/schedule/eckey.pem"
      }
    }
  ]
});

var container = CloudKit.getDefaultContainer();
var publicDB = container.publicCloudDatabase;

class StartWorker extends Subscription {
  static get schedule() {
    return {
      interval: "60m", // 1h 分钟间隔
      type: "all" // 指定所有的 worker 都需要执行
    };
  }

  async subscribe() {
    this.ctx.curl(
      "https://oapi.dingtalk.com/robot/send?access_token=a293b2e9a4ebf18882b7fb62b00ab4bce3d75eaddeed88ebd6799d763e4fb4e7",
      {
        method: "POST",
        contentType: "json",
        data: {
          msgtype: "text",
          text: {
            content: "(jd)测试定时器"
          }
        }
      }
    );
  }
}

module.exports = StartWorker;
